﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace WebApiServer.Extensions
{
    public static class AppExtensions
    {
        public static IApplicationBuilder UseDomainBase(this IApplicationBuilder app, IConfiguration configuration)
        {
            // refer this https://stackoverflow.com/questions/46593999/how-to-host-a-asp-net-core-application-under-a-sub-folder
            // because I will test to proxy this sub-area in nginx.
            var appOptions = configuration.GetSection("appOptions");
            var domainBase = appOptions["DomainBase"];
            if (!string.IsNullOrEmpty(domainBase))
            {
                var parts = domainBase.Split("/");
                if (parts.Any())
                {
                    var subDomainArea = parts.LastOrDefault();
                    app.UsePathBase($"/{subDomainArea}");
                }
            }
            return app;
        }
    }
}
