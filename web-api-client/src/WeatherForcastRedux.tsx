import axios from 'axios';
import { AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

export interface WeatherForecast {
    date: Date;
    temperatureC: number;
    temperatureF: number;
    summary: string;
}

export interface WeatherForecastState {
    isFetching: boolean;
    succeed: boolean;
    forecasts: WeatherForecast[] | undefined;
}

interface WeatherForecastResult {
    succeed: boolean;
    forecasts: WeatherForecast[];
}

export interface FetchingWeatherForecastsAction {
    type: string;
}

export interface FetchedWeatherForecastsAction {
    type: string;
    payload: WeatherForecastResult;
}

const FETCHING_FORECASTS = 'FETCHING_FORECASTS';
const FETCHED_FORECASTS = 'FETCHED_FORECASTS';

async function getWeatherForecasts(): Promise<WeatherForecastResult> {
    let baseUrl = `${process.env.REACT_APP_API_URL}`;
    let result: WeatherForecastResult;

    try {
        let response = await axios.get<WeatherForecast[]>(baseUrl);
        if (response.status == 200) {
            result = {
                succeed: true,
                forecasts: response.data
            };
        } else {
            let message = 'error when received data.';
            result = {
                succeed: false,
                forecasts: []
            };
            console.log(message);
        }
    } catch (error) {
        let message = 'error when receiving data.';
        result = {
            succeed: false,
            forecasts: []
        };
        console.log(message);
    }
    return result;
}

export function fetchWeatherForecasts(): ThunkAction<
    Promise<FetchedWeatherForecastsAction>,
    WeatherForecast[],
    unknown,
    FetchedWeatherForecastsAction
> {
    return async (dispatch: ThunkDispatch<
        WeatherForecast[],
        unknown,
        AnyAction>) => {
        let fetchingAction: FetchingWeatherForecastsAction = { type: FETCHING_FORECASTS };
        dispatch(fetchingAction);
        let forecastsResult = await getWeatherForecasts();
        let fetchedAction: FetchedWeatherForecastsAction = {
            type: FETCHED_FORECASTS,
            payload: forecastsResult
        };
        return dispatch(fetchedAction);
    };
}

const initState: WeatherForecastState = {
    isFetching: false,
    succeed: false,
    forecasts: undefined
};

export function WeatherForecastReducer(state = initState, action: any): WeatherForecastState {
    let result = initState;
    switch (action.type) {
        case FETCHING_FORECASTS:
            result = {
                ...state,
                isFetching: true
            };
            break;
        case FETCHED_FORECASTS:
            result = {
                ...state,
                isFetching: false,
                succeed: action.payload.succeed,
                forecasts: action.payload.forecasts
            };
            break;
        default:
            result = state;
            break;
    }
    return result;
}