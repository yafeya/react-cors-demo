import { WeatherForecastState } from "./WeatherForcastRedux";

export interface State{
    forecasts: WeatherForecastState;
}