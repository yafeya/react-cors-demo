import { connect } from "react-redux";
import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { State } from "./model";
import spinner from './spinner.png';
import { fetchWeatherForecasts, WeatherForecast } from "./WeatherForcastRedux";
import './WeatherForcast.scss'
import { useEffect } from "react";

export interface WeatherForcastProps {
    isFetching: boolean;
    succeed: boolean;
    forecasts: WeatherForecast[] | undefined;
    fetchWeatherForecasts: () => void;
}

export const WeatherForcast: React.FC<WeatherForcastProps> = (props) => {

    function isInitState() {
        return !props.isFetching && props.forecasts == undefined;
    }

    useEffect(() => {
        if (isInitState()) {
            props.fetchWeatherForecasts();
        }
    });

    return (
        <div className="weather-wrapper">
            <button className="btn btn-primary" onClick={props.fetchWeatherForecasts}>Refresh</button>
            <div className="weather-fetching-loader" data-is-fetching={props.isFetching}>
                <span>Loading</span>
            </div>
            <div className="weather-forecasts" data-is-fetching={props.isFetching}>
                {
                    props.forecasts?.map((item, index) =>
                        <div key={index} className="forecastWrapper" data-index={index % 2}>
                            <span>{`${item.summary} -- ${item.temperatureC} (${item.date})`}</span>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

const mapStateToProps = (state: State) => {
    const { isFetching, succeed, forecasts } = state.forecasts;
    return { isFetching, succeed, forecasts };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
    fetchWeatherForecasts: () => dispatch(fetchWeatherForecasts())
});

export const WeatcherForcastWrapper = connect(mapStateToProps, mapDispatchToProps)(WeatherForcast);