module.exports = {
    webpack: function (config, env) {
        // configure to dist
        console.log(`the env = ${env}`);
        if (env === 'production') {
            const path = require('path');
            const paths = require('react-scripts/config/paths');
            paths.appBuild = path.join(path.dirname(paths.appBuild), 'dist');
            config.output.path = path.join(path.dirname(config.output.path), 'dist');
        }
        return config;
    },
    // jest: function (config) {
    //   return config;
    // },
    devServer: function (configFunction) {
        return function (proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);
            config.disableHostCheck = true;
            config.historyApiFallback = true;
            return config;
        };
    },
    // paths: function (paths, env) {
    //   return paths;
    // },
}